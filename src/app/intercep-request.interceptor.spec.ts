import { TestBed } from '@angular/core/testing';

import { IntercepRequestInterceptor } from './intercep-request.interceptor';

describe('IntercepRequestInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      IntercepRequestInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: IntercepRequestInterceptor = TestBed.inject(IntercepRequestInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
