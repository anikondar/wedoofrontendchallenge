import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchCombination } from '../app.component';

@Injectable({
  providedIn: 'root'
})
export class SearchCombinationService {
  backendUrl = "http://localhost:3000/shop/"
  constructor(
    private http: HttpClient


  ) { }

  public getCombination(shopId: number,amount: number | undefined): Observable<SearchCombination>{
    return this.http.get(this.backendUrl + shopId +'/search-combination?amount='+ amount);
  }
}







