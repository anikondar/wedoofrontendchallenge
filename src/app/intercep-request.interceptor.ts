import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class IntercepRequestInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(httpRequest: HttpRequest<any>, httpHandler: HttpHandler): Observable<HttpEvent<any>> {
    //  request = request.clone();
    console.log("je suis la ");

  const  request = httpRequest.clone({
      setHeaders: {
        Authorization: 'tokenTest123'
      }
    });

    return httpHandler.handle(request);
  }
}
