import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SearchCombinationService } from './services/search-combination.service';

export interface SearchCombination {
  equal?: CalculatorComponentValue;
  ceil?: CalculatorComponentValue;
  floor?: CalculatorComponentValue;
}

export interface CalculatorComponentValue {
  value: number;
  cards: number[];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  @Output() emitAmount = new EventEmitter<number>();
  title = 'weedoTest';
  varToShow: SearchCombination = {};
  shopId: number = 5;
  desiredAmountForm: FormGroup;
  resultForm: FormGroup;

  constructor(
    private searchCombinationService: SearchCombinationService,
    private http: HttpClient,
    private fb: FormBuilder
  ) {
    this.desiredAmountForm = this.fb.group({
      desiredAmount: [0, [Validators.required]],
    });
    this.resultForm = this.fb.group({
      value: [0],
      cards: [0],
    });
  }

  ngOnInit(): void {}

  updateDesiredAmountForm(amount: number | undefined) {
    this.desiredAmountForm = this.fb.group({
      desiredAmount: [amount, [Validators.required]],
    });
  }

  updateResultForm(
    amount: number | undefined,
    cardsValues: number[] | undefined
  ) {
    this.resultForm = this.fb.group({
      value: [amount],
      cards: [cardsValues],
    });
  }

  onSubmit() {
    this.searchCombinationService
      .getCombination(this.shopId, this.desiredAmountForm.value.desiredAmount)
      .subscribe((response: SearchCombination) => {
        if (!response.equal && !response.floor) {
          this.updateDesiredAmountForm(response.ceil?.value);
          this.searchCombinationService
            .getCombination(
              this.shopId,
              this.desiredAmountForm.value.desiredAmount
            )
            .subscribe((response: SearchCombination) => {
              this.updateDesiredAmountForm(response.equal?.value);
              this.emitAmount.emit(this.desiredAmountForm.value.desiredAmount);
              this.varToShow = response;
              this.updateResultForm(
                response.equal?.value,
                response.equal?.cards
              );
            });
        } else if (!response.equal && !response.ceil) {
          this.desiredAmountForm.value.desiredAmount = response.floor?.value;
          this.searchCombinationService
            .getCombination(
              this.shopId,
              this.desiredAmountForm.value.desiredAmount
            )
            .subscribe((response: SearchCombination) => {
              this.updateDesiredAmountForm(response.equal?.value);
              this.emitAmount.emit(this.desiredAmountForm.value.desiredAmount);
              this.varToShow = response;
              this.updateResultForm(
                response.equal?.value,
                response.equal?.cards
              );
            });
        } else {
          this.varToShow = response;
          this.updateResultForm(response.equal?.value, response.equal?.cards);
          this.emitAmount.emit(this.desiredAmountForm.value.desiredAmount);
        }
      });
  }

  updateSubmit(amount: number | undefined) {
    this.updateDesiredAmountForm(amount);
    this.searchCombinationService
      .getCombination(this.shopId, this.desiredAmountForm.value.desiredAmount)
      .subscribe((response: SearchCombination) => {
        this.emitAmount.emit(this.desiredAmountForm.value.desiredAmount);
        this.varToShow = response;
        this.updateResultForm(response.equal?.value, response.equal?.cards);
      });
  }

  onNext() {
    this.searchCombinationService
      .getCombination(this.shopId, this.desiredAmountForm.value.desiredAmount)
      .subscribe((response: SearchCombination) => {
        this.emitAmount.emit(this.desiredAmountForm.value.desiredAmount);
        this.varToShow = response;
        this.updateResultForm(response.equal?.value, response.equal?.cards);

        if (
          this.varToShow?.equal &&
          this.varToShow?.ceil &&
          this.varToShow?.equal?.value == this.varToShow?.ceil?.value
        ) {
          this.updateDesiredAmountForm(this.varToShow?.ceil?.value + 1);
        } else if (
          !this.varToShow?.equal?.value &&
          this.varToShow?.ceil?.value
        ) {
          this.updateDesiredAmountForm(this.varToShow.ceil.value);
        }
        this.searchCombinationService
          .getCombination(
            this.shopId,
            this.desiredAmountForm.value.desiredAmount
          )
          .subscribe((response: SearchCombination) => {
            if (!response.equal && !response.ceil) {
              this.searchCombinationService
                .getCombination(this.shopId, response.floor?.value)
                .subscribe((responses: SearchCombination) => {
                  this.updateDesiredAmountForm(responses.equal?.value);
                  this.emitAmount.emit(
                    this.desiredAmountForm.value.desiredAmount
                  );
                  this.varToShow = responses;
                  this.updateResultForm(
                    responses.equal?.value,
                    responses.equal?.cards
                  );
                });
            } else {
              if(!response.equal){
                this.searchCombinationService
                .getCombination(this.shopId, response.ceil?.value)
                .subscribe((responses: SearchCombination) => {
                  this.updateDesiredAmountForm(responses.equal?.value);
                  this.emitAmount.emit(
                    this.desiredAmountForm.value.desiredAmount
                  );
                  this.varToShow = responses;
                  this.updateResultForm(
                    responses.floor?.value,
                    responses.floor?.cards
                  );
                });
              } else {
                this.emitAmount.emit(this.desiredAmountForm.value.desiredAmount);
                this.varToShow = response;
                this.updateResultForm(
                  response.equal?.value,
                  response.equal?.cards
                );
              }
            }
          });
      });
  }

  onPrevious() {
    this.searchCombinationService
      .getCombination(this.shopId, this.desiredAmountForm.value.desiredAmount)
      .subscribe((response: SearchCombination) => {
        this.varToShow = response;
        this.updateResultForm(response.equal?.value, response.equal?.cards);
        if (
          this.varToShow?.equal &&
          this.varToShow?.floor &&
          this.varToShow?.equal?.value == this.varToShow?.floor?.value
        ) {
          this.updateDesiredAmountForm(this.varToShow?.floor?.value - 1);
        } else if (
          !this.varToShow?.equal?.value &&
          this.varToShow?.floor?.value
        ) {
          this.updateDesiredAmountForm(this.varToShow.floor.value);
        }
        this.searchCombinationService
          .getCombination(
            this.shopId,
            this.desiredAmountForm.value.desiredAmount
          )
          .subscribe((response: SearchCombination) => {
            if (!response.equal && !response.floor) {
              this.searchCombinationService
                .getCombination(this.shopId, response.ceil?.value)
                .subscribe((responses: SearchCombination) => {
                  this.updateDesiredAmountForm(responses.equal?.value);
                  this.emitAmount.emit(
                    this.desiredAmountForm.value.desiredAmount
                  );
                  this.varToShow = responses;
                  this.updateResultForm(
                    responses.ceil?.value,
                    responses.ceil?.cards
                  );
                });
            } else {
              if(!response.equal){
                this.searchCombinationService
                .getCombination(this.shopId, response.floor?.value)
                .subscribe((responses: SearchCombination) => {
                  this.updateDesiredAmountForm(responses.equal?.value);
                  this.emitAmount.emit(
                    this.desiredAmountForm.value.desiredAmount
                  );
                  this.varToShow = responses;
                  this.updateResultForm(
                    responses.equal?.value,
                    responses.equal?.cards
                  );
                });
              } else {
              this.varToShow = response;
              this.updateResultForm(
                response.equal?.value,
                response.equal?.cards
              );
              this.emitAmount.emit(this.desiredAmountForm.value.desiredAmount);
              }
            }
          });
      });
  }
}
